package dto;

/**
 * 
 * @author guillaumeSauv
 * 
 *
 */
public class Ville {

	private String insee;
	private String codePostal;
	private String nom;

	/**
	 * 
	 * @param insee
	 *          de la ville
	 * @param codePostale
	 *          de la ville
	 * @param nom
	 *          de la ville
	 */

	public Ville(String insee, String codePostal, String nom) {

		this.insee = insee;
		this.codePostal = codePostal;
		this.nom = nom;
	}

	public String getInsee() {
		return insee;
	}

	public void setInsee(String insee) {
		this.insee = insee;
	}

	public String getCodePostal() {
		return codePostal;
	}

	public void setCodePostal(String codePostal) {
		this.codePostal = codePostal;
	}

	public String getNom() {
		return nom;
	}

	public void setNom(String nom) {
		this.nom = nom;
	}

	@Override
	public String toString() {
		return "Ville [INSEE=" + insee + ", Nom=" + nom + ", CP=" + codePostal + "]";
	}
}
