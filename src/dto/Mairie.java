package dto;

import org.apache.commons.lang.WordUtils;
import org.w3c.dom.Element;
import org.w3c.dom.NodeList;
/**
 * 
 * @author gpillet
 *
 */
public class Mairie {

	private Ville ville;
	private String nom;
	private String horaire;
	private String adresse;
	private String phone;
	private String fax;
	private String mail;
	private String siteWeb;

	/**
	 * @param Ville
	 *          de la mairie
	 * @param nom
	 *          de la mairie
	 * @param horraire
	 *          de la mairie
	 * @param adresse
	 *          de la mairie
	 * @param phone
	 *          telephone de la mairie
	 * @param fax
	 *          numero de fax de la mairie
	 * @param mail
	 *          adresse mail de la mairie
	 * @param siteWeb
	 *          de la mairie
	 *
	 */

	public Mairie(Ville ville, String nom, String adresse, String phone, String fax, String mail, String siteWeb,
	    String horraire) {

		this.ville = ville;
		this.nom = nom;
		this.horaire = horraire;
		this.adresse = adresse;
		this.phone = phone;
		this.fax = fax;
		this.mail = mail;
		this.siteWeb = siteWeb;
	}

	public Ville getVille() {
		return ville;
	}

	public void setVille(Ville ville) {
		this.ville = ville;
	}

	public String getHoraire() {
		return horaire;
	}

	public void setHoraire(String horaire) {
		this.horaire = horaire;
	}

	public String getNom() {
		return nom;
	}

	public void setNom(String nom) {
		this.nom = nom;
	}

	public String getAdresse() {
		return adresse;
	}

	public void setAdresse(String adresse) {
		this.adresse = adresse;
	}

	public String getPhone() {
		return phone;
	}

	public void setPhone(String phone) {
		this.phone = phone;
	}

	public String getFax() {
		return fax;
	}

	public void setFax(String fax) {
		this.fax = fax;
	}

	public String getMail() {
		return mail;
	}

	public void setMail(String mail) {
		this.mail = mail;
	}

	public String getSiteWeb() {
		return siteWeb;
	}

	public void setSiteWeb(String siteWeb) {
		this.siteWeb = siteWeb;
	}

	/**
	 * Redéfinition de la méthode toString permettant de définir la traduction de
	 * l'objet en String pour affichage par exemple
	 */
	@Override
	public String toString() {
		String mairie = ville.toString() + "\n";
		mairie += "Nom : " + this.getNom() + "\n";
		mairie += "Adresse : " + this.getAdresse() + "\n";
		mairie += "Téléphone : " + this.getPhone() + "\n";
		mairie += "Fax : " + this.getFax() + "\n";
		mairie += "Email : " + this.getMail() + "\n";
		mairie += "Url : " + this.getSiteWeb() + "\n";
		mairie += "Horaire : \n" + this.getHoraire();

		return mairie;
	}
}
