package controleur;

import com.fasterxml.jackson.databind.ObjectMapper;

import dao.MairieDAO;
import dao.VilleDAO;
import dto.Mairie;
import dto.Ville;


import java.util.List;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * Servlet implementation class RechercheServlet
 * @author gpillet
 */
@WebServlet("/RechercheServlet")
public class RechercheServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
	private MairieDAO mairieDao;
	private VilleDAO villeDao;

	/**
	 * @see HttpServlet#HttpServlet()
	 */
	public RechercheServlet() {
		super();
		mairieDao = MairieDAO.getInstance();
		villeDao = VilleDAO.getInstance();
	}

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		List<Ville> villes = villeDao.getVilleByINSEE(request.getParameter("insee"));
		ObjectMapper mapper = new ObjectMapper();
		if (villes != null && villes.size() > 0) {
			Ville ville = villes.get(0);
			Mairie mairie = mairieDao.get(ville, getServletContext().getRealPath("/"));
			response.setCharacterEncoding("UTF-8");
			if (mairie == null) {
				response.getWriter().append(null);
			} else {
				response.getWriter().append(mapper.writeValueAsString(mairie));
			}
		}
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
		doGet(request, response);
	}

}
