package dao;

import dto.*;

import java.io.File;
import java.io.FileNotFoundException;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;

import org.apache.commons.lang.WordUtils;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

/**
 * Classe d'accès aux données contenues dans la table message
 * 
 * @author gpillet
 */
public class MairieDAO {

	/**
	 * singleton attribut permettant de mettre en oeuvre le design pattern
	 * singleton
	 */
	private static volatile MairieDAO singleton;

	/**
	 * Constructeur (privé) de la classe Privé car utilisation du design pattern
	 * singleton
	 */
	private MairieDAO() {
	}

	/**
	 * Permet de récupérer l'instance unique de la classe MessageDAO cf design
	 * pattern singleton
	 * 
	 * @return
	 */
	public static MairieDAO getInstance() {
		synchronized (MairieDAO.class){
			if (MairieDAO.singleton == null)
				singleton = new MairieDAO();
		}
		return singleton;
	}

	/**
	 * 
	 * @param insee
	 *          Code insee utilisé pour trouver la mairie voulu
	 * @return La mairie de la ville correspondant au code insee
	 */
	public Mairie get(Ville ville, String rootPath) {
		String insee = ville.getInsee();
		try {
			File fXmlFile = new File(rootPath + "WEB-INF" + File.separator + "ressources" + File.separator + "Mairies"
			    + File.separator + "mairie-" + insee + "-01.xml");
			DocumentBuilderFactory dbFactory = DocumentBuilderFactory.newInstance();
			DocumentBuilder dBuilder = dbFactory.newDocumentBuilder();
			Document doc = dBuilder.parse(fXmlFile);

			doc.getDocumentElement().normalize();

			NodeList nList = doc.getElementsByTagName("Organisme");

			for (int temp = 0; temp < nList.getLength(); temp++) {

				Node nNode = nList.item(temp);

				if (nNode.getNodeType() == Node.ELEMENT_NODE) {

					Element eElement = (Element) nNode;
					String horraire = "";
					String fax;
					String nom;
					String adresse;
					String phone;
					String mail;
					String siteWeb;
					try {
						nom = eElement.getElementsByTagName("Nom").item(0).getTextContent();
					} catch (NullPointerException e) {
						nom = "NON RENSEIGNE";
					}
					try {
						adresse = eElement.getElementsByTagName("Ligne").item(0).getTextContent() + "<br/>          "
						    + eElement.getElementsByTagName("CodePostal").item(0).getTextContent() + ", "
						    + eElement.getElementsByTagName("NomCommune").item(0).getTextContent();
					} catch (NullPointerException e) {
						adresse = "NON RENSEIGNE";
					}
					try {
						phone = eElement.getElementsByTagName("Téléphone").item(0).getTextContent();
					} catch (NullPointerException e) {
						phone = "NON RENSEIGNE";
					}
					try {
						fax = eElement.getElementsByTagName("Télécopie").item(0).getTextContent();
					} catch (NullPointerException e) {
						fax = "NON RENSEIGNE";
					}
					try {
						mail = eElement.getElementsByTagName("Email").item(0).getTextContent();
					} catch (NullPointerException e) {
						mail = "NON RENSEIGNE";
					}
					try {
						siteWeb = eElement.getElementsByTagName("Url").item(0).getTextContent();
					} catch (NullPointerException e) {
						siteWeb = "NON RENSEIGNE";
					}
					try {
						NodeList eJourList = eElement.getElementsByTagName("PlageJ");
						for (int i = 0; i < eJourList.getLength(); i++) {
							Element eJour = (Element) eJourList.item(i);
							String debut = eJour.getAttribute("début"), fin = eJour.getAttribute("fin");
							if (debut.equals(fin)) {
								horraire += "Le " + WordUtils.capitalize(debut) + "</br>";
							} else {
								horraire += "Du : " + WordUtils.capitalize(debut) + " au " + WordUtils.capitalize(fin) + "</br>";
							}
							Element eHeure = (Element) eJour.getElementsByTagName("PlageH").item(0);
							horraire += "De : " + eHeure.getAttribute("début") + " au " + eHeure.getAttribute("fin") + "</br>";
						}
					} catch (NullPointerException e) {
						horraire = "NON RENSEIGNE";
					}
					Mairie mairie = new Mairie(ville, nom, adresse, phone, fax, mail, siteWeb, horraire);
					return mairie;
				}
			}
		} catch (FileNotFoundException e) {
			System.out.println("File Not found");
			e.printStackTrace();
		} catch (Exception e) {
			e.printStackTrace();
		}
		return null;

	}

}
