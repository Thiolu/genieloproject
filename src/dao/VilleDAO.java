package dao;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.List;
import dto.Ville;;

/**
 * 
 * @author guillaumeSauv
 *
 */
public class VilleDAO {

	/**
	 * Paramètres de connexion à la base de données
	 */
	final static String URL = "jdbc:mysql://localhost/cityhall";
	final static String LOGIN = "root";
	final static String PASS = "root";

	/**
	 * singleton attribut permettant de mettre en oeuvre le design pattern
	 * singleton
	 */
	private static volatile VilleDAO singleton;

	/**
	 * Constructeur (privé) de la classe Privé car utilisation du design pattern
	 * singleton
	 */
	private VilleDAO() {
		// chargement du pilote Mysql
		try {
			Class.forName("com.mysql.jdbc.Driver");
		} catch (ClassNotFoundException e2) {
			System.out.println(
			    "Impossible de charger le pilote de BDD, ne pas oublier d'importer le fichier mysql-connector-java-XXXX.jar dans le projet");
		}

	}

	/**
	 * Permet de récupérer l'instance unique de la classe VilleDAO cf design
	 * pattern singleton
	 * 
	 * @return
	 */
	public static VilleDAO getInstance() {
		synchronized (VilleDAO.class){
			if (VilleDAO.singleton == null)
				singleton = new VilleDAO();
		}
		return singleton;
	}

	/**
	 * Permet de récupérer une liste de ville par INSEE
	 * 
	 * @param INSEE
	 * @return liste de ville
	 * @return null si aucun message ne correspond à cet INSEE
	 */
	public List<Ville> getVilleByINSEE(String INSEE) {
		if (INSEE != "") {
			Connection con = null;
			PreparedStatement ps = null;
			ResultSet rs = null;
			List<Ville> retour = new ArrayList<Ville>();

			// connexion a la base de données
			try {

				con = DriverManager.getConnection(URL, LOGIN, PASS);
				ps = con.prepareStatement("SELECT DISTINCT * FROM ville WHERE INSEE LIKE ? GROUP BY INSEE");
				ps.setString(1, INSEE + "%");

				// on execute la requete
				rs = ps.executeQuery();
				// on parcourt les lignes du resultat
				while (rs.next())
					retour.add(new Ville(rs.getString("INSEE"), rs.getString("CodePostal"), rs.getString("Nom")));

			} catch (Exception ee) {
				ee.printStackTrace();
			} finally {
				// fermeture du rs,preparedStatement et de la connexion
				try {
					if (rs != null)
						rs.close();
				} catch (Exception t) {
				}
				try {
					if (ps != null)
						ps.close();
				} catch (Exception t) {
				}
				try {
					if (con != null)
						con.close();
				} catch (Exception t) {
				}
			}
			return retour;
		} else {
			return null;
		}
	}

	/**
	 * Permet de récupérer une liste de ville par Code postal
	 * 
	 * @param CP
	 *          le code postale
	 * @return liste de ville
	 * @return null si aucun message ne correspond à ce Code Postal
	 */

	public List<Ville> getVilleByCP(String CP) {

		Connection con = null;
		PreparedStatement ps = null;
		ResultSet rs = null;
		List<Ville> retour = new ArrayList<Ville>();

		// connexion a la base de données
		try {

			con = DriverManager.getConnection(URL, LOGIN, PASS);
			ps = con.prepareStatement("SELECT * FROM ville WHERE CodePostal LIKE ? GROUP BY INSEE");
			ps.setString(1, CP + "%");

			// on execute la requete
			rs = ps.executeQuery();
			// on parcourt les lignes du resultat
			while (rs.next())
				retour.add(new Ville(rs.getString("INSEE"), rs.getString("CodePostal"), rs.getString("Nom")));

		} catch (Exception ee) {
			ee.printStackTrace();
		} finally {
			// fermeture du rs,preparedStatement et de la connexion
			try {
				if (rs != null)
					rs.close();
			} catch (Exception t) {
			}
			try {
				if (ps != null)
					ps.close();
			} catch (Exception t) {
			}
			try {
				if (con != null)
					con.close();
			} catch (Exception t) {
			}
		}
		return retour;

	}

	/**
	 * Permet de récupérer une liste de ville par Nom
	 * 
	 * @param Nom
	 * @return liste de ville
	 * @return null si aucun message ne correspond à ce Non
	 */
	public List<Ville> getVilleByNom(String nom) {

		Connection con = null;
		PreparedStatement ps = null;
		ResultSet rs = null;
		List<Ville> retour = new ArrayList<Ville>();

		// connexion a la base de données
		try {

			con = DriverManager.getConnection(URL, LOGIN, PASS);
			ps = con.prepareStatement("SELECT DISTINCT * FROM ville WHERE Nom LIKE ? GROUP BY INSEE");
			ps.setString(1, nom + "%");

			// on execute la requete
			rs = ps.executeQuery();
			// on parcourt les lignes du resultat
			while (rs.next())
				retour.add(new Ville(rs.getString("INSEE"), rs.getString("CodePostal"), rs.getString("Nom")));

		} catch (Exception ee) {
			ee.printStackTrace();
		} finally {
			// fermeture du rs,preparedStatement et de la connexion
			try {
				if (rs != null)
					rs.close();
			} catch (Exception t) {
			}
			try {
				if (ps != null)
					ps.close();
			} catch (Exception t) {
			}
			try {
				if (con != null)
					con.close();
			} catch (Exception t) {
			}
		}
		return retour;

	}

	// main permettant de tester la classe
	public static void main(String[] args) {

		VilleDAO villeDAO = new VilleDAO();

		List<Ville> liste = villeDAO.getVilleByINSEE("76000");
		System.out.println(liste);

		System.out.println("----------------------");
		List<Ville> liste1 = villeDAO.getVilleByCP("27170");
		System.out.println(liste1);

		System.out.println("----------------------");
		List<Ville> liste2 = villeDAO.getVilleByNom("ROUEN");
		System.out.println(liste2);

	}

}
