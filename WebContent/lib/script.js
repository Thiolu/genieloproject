var context;
$(document).ready(
    function() {
	    var termTemplate = "<span class='ui-autocomplete-term'>%s</span>";
	    $("#villes").autocomplete(
	        {
	          source : function(request, response) {
		          $.ajax({
		            url : "getData.jsp",
		            type : "GET",
		            dataType : "text",
		            data : {
			            term : request.term
		            },
		            success : function(data) {
			            data = data.replace(/\r?\n|\r/g, '');
			            data = data.split("|");

			            response($.map(data, function(items) {
				            var list = items.split(";");
				            var item = {
				              name : list[0],
				              value : list[1]
				            }
				            if (item.name == "")
					            return;
				            return {
				              label : item.name.trim(),
				              value : item.value,
				            }
			            }));
		            },
		            error : function(error) {
			            alert('error: ' + error);
		            }
		          });
	          },
	          open : function(e, ui) {
		          var acData = $(this).data('ui-autocomplete');
		          acData.menu.element.find('li').each(
		              function() {
			              var me = $(this);
			              var keywords = acData.term.split(' ').join('|');
			              me.html(me.text().replace(
			                  new RegExp("(" + keywords + ")", "gi"), '<b>$1</b>'));
		              });
	          },
	          minLength : 3,
	          select : function(event, ui) {
		          var item = ui.item;
		          $('#insee').val(item.value);
		          this.value = item.label;
		          getMairie();
		          return false;
	          },
	          focus : function(event, ui) {
		          var item = ui.item;
		          $('#insee').val(item.value);
		          this.value = item.label;
		          return false;
	          }
	        });
    });
function setContext(data){
	context = data;
}
function getMairie() {
	$.ajax({
	  url : context+"/RechercheServlet",
	  type : "GET",
    dataType: 'json',
	  data : {
		  insee : $('#insee').val()
	  },
	  success : function(mairie) {
	  	var ville = mairie.ville;
	  	$("#mairie").css("display","inline");
		  $("#NomVille").html(ville.nom);
		  $("#CP").html(ville.codePostal);
		  $("#inseeCode").html(ville.insee);
		  $("#NomMairie").html(mairie.nom);
		  $("#AdresseMairie").html(mairie.adresse);
		  $("#phone").html(mairie.phone);
		  $("#fax").html(mairie.fax);
		  if(mairie.mail !== "NON RENSEIGNE"){
			  $("#mail").html("<a href:\""+mairie.mail+"\">"+mairie.mail+"</a>");
		  }else{
			  $("#mail").html(mairie.siteWeb);
		  }
		  if(mairie.siteWeb !== "NON RENSEIGNE"){
			  $("#URL").html("<a href:\""+mairie.siteWeb+"\">"+mairie.siteWeb+"</a>");
		  }else{
			  $("#URL").html(mairie.siteWeb);
		  }
		  $("#horaire").html(mairie.horaire);
		  
	  },
	  error : function(error) {
	  	$("#mairie").css("display","none");
		  alert('error: ' + error.responseText);
	  }
	});
}
