<%@page import="dto.Ville"%>
<%@page import="dao.VilleDAO"%>
<%@page import="java.util.Iterator"%>
<%@page import="java.util.List"%>

<%
	VilleDAO vDAO = VilleDAO.getInstance();

	String search = request.getParameter("term");

	List<Ville> villes = vDAO.getVilleByINSEE(search);
	villes.addAll(vDAO.getVilleByCP(search));
	villes.addAll(vDAO.getVilleByNom(search));

	Iterator<Ville> iterator = villes.iterator();
	int i = 0;
	while (iterator.hasNext()) {
		Ville v = (Ville) iterator.next();
		out.print(v.getNom() + ";" + v.getInsee() + "|");
	}
%>