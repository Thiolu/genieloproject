<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<!DOCTYPE html>
<%@ page import="dao.*,dto.*,java.util.*"%>
<html>
<meta charset="UTF-8">
<link rel="stylesheet" href="lib/bootstrap.min.css">
<script src="lib/jquery.js"></script>
<script src="lib/jquery-ui.js"></script>
<script src="lib/script.js"></script>
<link rel="stylesheet" type="text/css" href="lib/jquery-ui.css" />
<style>
h1 {
	color: #9E97ff;
}

small {
	color: #FF94C4;
}

#villes {
	color: #e8c089;
	border-style: solid;
	border-color: #9E97FF;
	border-width: 0px 0px 5px 0px;
}

.centerText {
	text-align: center;
}

.ui-autocomplete-term {
	font-weight: bold;
	color: #89E8D2;
}
#mairie{
display:none;
}
</style>
<title>CityHall Search</title>
<body onload="setContext('<% out.print(request.getContextPath());%>')">
	<div class="container">
		<div class="title">
				<h1>CityHall Search</h1>
			<p style="padding-left: 48%">Trouvez les informations de
				votre mairie!</p>
		</div>
		<br> <br>
		<center>
			<input type="text" class="form-control" name="villes" id="villes"
				onkeydown="if(event.keyCode==13) return getMairie();"
				placeholder="Nom de commune, code postal, code INSEE, ...">
			<input type="hidden" name="insee" id="insee">
		</center>
		<div id="mairie">
			<div class="container jumbotron">
				<div id="villeDetail" class="col-md-6 ville_detail">
					<h2 id="NomVille"></h2>
					<div>
						Code Postal : <span id="CP"></span><br>Code INSEE :  <span id="inseeCode"></span>
					</div>
				</div>
				<div class="col-md-6 mairie_detail">
					<h2>Contact</h2>
					<p>Nom : <span id="NomMairie"></span></p>
					<p>
						Adresse : <span id="AdresseMairie"></span>
					</p>
					<p>
						Téléphone : <span id="phone"></span>
					</p>
					<p>Fax : <span id="fax"></span></p>
					<p>Email : <span id="mail"></span></p>
					<p>Url : <span id="URL"></span></p>
				</div>
				<div class="col-md-12 centerText">
					<h2>Horaire</h2>
					<span id="horaire"></span>
				</div>
			</div>
		</div>
</body>
</html>